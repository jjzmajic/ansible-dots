set nocompatible
let cache_plug_path=stdpath('cache') . '/plugged'
let autoload_plug_path=stdpath('config') . '/autoload/plug.vim'
if !filereadable(autoload_plug_path)
  silent exe '!curl -fL --create-dirs -o ' . autoload_plug_path .
        \ ' https://raw.github.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin(cache_plug_path)
" vim enhancements
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-vinegar'
Plug 'tpope/vim-sleuth'
Plug 'tpope/vim-rsi'
" pairing
Plug 'jiangmiao/auto-pairs'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-endwise'
" text objects
Plug 'wellle/targets.vim'
Plug 'kana/vim-textobj-user'
Plug 'kana/vim-textobj-line'
Plug 'kana/vim-textobj-entire'
" completion
Plug 'Shougo/denite.nvim',
      \ { 'do': ':UpdateRemotePlugins' }
Plug 'neoclide/coc.nvim',
      \ {'tag': '*', 'do': { -> coc#util#install()}}
Plug 'neoclide/coc-denite'
Plug 'neoclide/denite-extra'
Plug 'liuchengxu/vim-which-key'
" rice
Plug 'dylanaraps/wal.vim'
Plug 'vim-airline/vim-airline'
Plug 'ryanoasis/vim-devicons'
call plug#end()
