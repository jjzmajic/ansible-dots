#!/bin/sh

devmon &
xss-lock slock &
lxqt-policykit-agent &
syncthing -no-browser &
emacs --bg-daemon

# touchpad
synclient TapButton1=1
synclient TapButton2=3
synclient TapButton3=2
synclient HorizTwoFingerScroll=1
synclient VertTwoFingerScroll=1

# x settings
xsetroot -cursor_name left_ptr
setxkbmap -option caps:swapescape
