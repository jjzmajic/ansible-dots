;;; docker+.el -*- lexical-binding: t; -*-

(use-package dockerfile-mode :defer t)

(provide 'docker+)
;;; docker+.el ends here
