;;; progrice+.el -*- lexical-binding: t; -*-

(use-package highlight-indent-guides
  :ghook 'prog-mode-hook
  :init (setq highlight-indent-guides-method 'column)
  :general (kbd+ "th" '(highlight-indent-guides-mode
                        :wk "highlight indentation")))

(use-package rainbow-delimiters
  :ghook '(prog-mode-hook conf-mode-hook))
(use-package highlight-numbers
  :ghook '(prog-mode-hook conf-mode-hook))
(use-package hl-todo
  :ghook '(prog-mode-hook conf-mode-hook))

(provide 'progrice+)
;;; progrice+.el ends here
