;;; cl+.el -*- lexical-binding: t; -*-

;;; roswell
(defvar cl+ "ros -Q run")

;;; packages
(use-package sly
  :ghook 'lisp-mode-hook
  :init
  (setq inferior-lisp-program cl+
        sly-kill-without-query-p t)
  :init
  (list+ 'sly-contribs 'sly-fancy)
  (list+ 'sly-contribs 'sly-mrepl)
  :general
  (kbd+local
    :keymaps 'sly-mode-map
    "e" '(:wk "eval")
    kbd+localleader #'sly-eval-last-expression)

  (kbd+local
    :keymaps 'sly-mode-map
    :infix "e"
    "f" '(sly-eval-defun :wk "function")
    "r" '(sly-eval-region :wk "region"))
  :config
  (sly-setup)

  (after+ 'handle
    (handle '(lisp-mode sly-mrepl-mode)
            :evaluators #'sly-eval-buffer
            :docs #'sly-describe-symbol
            :gotos #'sly-edit-definition
            :repls #'sly))

  (after+ 'compdef
    (compdef
     :modes '(lisp-mode sly-mrepl-mode)
     :company '(company-capf
                company-dabbrev)
     :capf '(sly-complete-filename-maybe
             sly-complete-symbol)))

  (after+ 'helm-company
    (general-def
      :keymaps 'sly-mrepl-mode-map
      [tab] #'indent-for-tab-command))

  (after+ 'evil+
    (hook+
     '(sly-db-mode-hook
       sly-inspector-mode-hook
       sly-popup-buffer-mode-hook
       sly-xref-mode-hook)
     #'evil-normal-state)))

(use-package sly-macrostep
  :after sly
  :init (list+ 'sly-contribs 'sly-macrostep)
  :general (kbd+local :keymaps 'sly-mode-map
             "m" #'macrostep-expand))

(provide 'cl+)
;;; cl+.el ends here
