;;; golang+.el -*- lexical-binding: t; -*-

(use-package go-mode
  :init
  (when (executable-find "gogetdoc")
    (setq godoc-at-point-function
          #'godoc-gogetdoc))
  (after+ 'lsp+
    (hook+ 'go-mode-hook
           #'lsp-deferred))
  :general
  (kbd+local
    :keymaps 'go-mode-map
    "a" #'go-goto-arguments
    "d" #'go-goto-docstring
    "f" #'go-goto-function-name
    "v" #'go-goto-return-values
    "r" #'go-goto-method-receiver))

(use-package gorepl-mode
  :ghook 'go-mode-hook
  :config (after+ 'crux
            (crux-with-region-or-buffer
             gorepl-eval-region)))

(use-package helm-go-package
  :after helm
  :general (kbd+local :keymaps 'go-mode-map
             "i" #'helm-go-package))

(after+ 'handle
  (handle '(go-mode gorepl-mode)
          :formatters #'gofmt
          :repls #'gorepl-run
          :evaluators #'gorepl-eval-region
          :gotos #'godef-jump-other-window
          :docs #'godoc-at-point))

(provide 'golang+)
;;; golang+.el ends here
