;;; util+.el -*- lexical-binding: t; -*-

(defalias 'list+ #'add-to-list)
(defalias 'hook+ #'general-add-hook)
(defalias 'advice+ #'general-add-advice)
(defalias 'after+ #'general-with-eval-after-load)

(defun enlist+ (exp)
  (declare (pure t) (side-effect-free t))
  (if (listp exp) exp (list exp)))

(defmacro maybe+ (fun &rest args)
  `(when (fboundp ,fun)
     (apply ,fun ,args)))

(provide 'util+)
;;; util+.el ends here
