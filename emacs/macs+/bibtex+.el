;;; bibtex+.el -*- lexical-binding: t; -*-

;;; vars
(defvar bibtex+bibliography (dirs+sync "papers/references.bib"))
(defvar bibtex+notes (dirs+sync "papers/notes/"))
(defvar bibtex+pdfs (dirs+sync "papers/pdfs/"))
(defvar bibtex+ (if (executable-find "biber")
                    'biblatex 'BibTeX))

;; TODO: match current line too.
(defmacro bibtex+unhelm (helm-fun new-fun)
  `(defalias ,new-fun
     (lambda nil
       (interactive)
       (funcall ,helm-fun
		(enlist+ (progn
			   (re-search-backward
			    bibtex-entry-head nil t)
			   (bibtex-key-in-head)))))))

;;; packages
(use-package bibtex
  :defer t
  :init (setq-default bibtex-dialect bibtex+)
  :config (after+ 'handle
	    (handle #'bibtex-mode
		    :formatters #'bibtex-reformat)))

(use-package helm-bibtex
  :after helm+
  :commands
  bibtex-completion-edit-notes
  bibtex-completion-open-pdf
  :init
  (setq bibtex-completion-bibliography bibtex+bibliography
        bibtex-completion-library-path bibtex+pdfs
        bibtex-completion-notes-path bibtex+notes
        helm-bibtex-full-frame nil)
  :general
  (kbd+local
    :keymaps 'bibtex-mode-map
    "g" '(helm-bibtex :wk "global bibliography")
    "l" '(helm-bibtex-with-local-bibliography
          :wk "local bibliography")))

(use-package biblio
  :defer t
  :init (setq biblio-download-directory bibtex+pdfs)
  :gfhook ('biblio-selection-mode-hook
           #'evil-motion-state))

(use-package reftex
  :init
  (setq reftex-plug-into-AUCTeX t
        reftex-default-bibliography
        bibtex+bibliography)
  :defer t)

(use-package company-reftex
  :after company)

(kbd+local :keymaps 'bibtex-mode-map
  "n" '(bibtex+notes :wk "notes")
  "p" '(bibtex+pdf :wk "pdf"))
(bibtex+unhelm
 #'bibtex-completion-edit-notes
 #'bibtex+notes)
(bibtex+unhelm
 #'bibtex-completion-open-pdf
 #'bibtex+pdf)

(provide 'bibtex+)
;;; bibtex+.el ends here
