;;; wm+.el -*- lexical-binding: t; -*-

;;; vars
(defvar wm+0-command
  #'dired-sidebar-jump-to-sidebar)

(defun wm+wk nil
  (push '((nil . "select-window-[1-9]")
          . t) which-key-replacement-alist)
  (push '((nil . "winum-buffer-to-window-[2-9]")
          . t) which-key-replacement-alist)
  (push '(("\\(.*\\) 1" . "winum-buffer-to-window-1")
          . ("\\1 1..9" . "buffer 1..9"))
        which-key-replacement-alist)
  (push `(("\\(.*\\) 0" . ,(symbol-name wm+0-command))
          . ("\\1 0..9" . "window 0..9"))
        which-key-replacement-alist))

;;; packages
(use-package winum
  :straight
  (winum :type git
         :host github
         :repo "jjzmajic/emacs-winum")
  :defer nil
  :config
  (after+ 'which-key
    (wm+wk))
  (winum-mode +1)
  :general
  (kbd+
    "1" #'winum-select-window-1
    "2" #'winum-select-window-2
    "3" #'winum-select-window-3
    "4" #'winum-select-window-4
    "5" #'winum-select-window-5
    "6" #'winum-select-window-6
    "7" #'winum-select-window-7
    "8" #'winum-select-window-8
    "9" #'winum-select-window-9)
  (kbd+
    :infix "b"
    "1" #'winum-buffer-to-window-1
    "2" #'winum-buffer-to-window-2
    "3" #'winum-buffer-to-window-3
    "4" #'winum-buffer-to-window-4
    "5" #'winum-buffer-to-window-5
    "6" #'winum-buffer-to-window-6
    "7" #'winum-buffer-to-window-7
    "8" #'winum-buffer-to-window-8
    "9" #'winum-buffer-to-window-9))

(use-package windower
  :straight
  (windower
   :host gitlab
   :repo "ambrevar/emacs-windower")
  :general
  (general-def
    :prefix-map 'wm+hercules
    "L" '(windower-move-border-right :wk "move border right")
    "H" '(windower-move-border-left :wk "move border left")
    "J" '(windower-move-border-below :wk "move border down")
    "K" '(windower-move-border-above :wk "move border up")))

(use-package winner
  :straight nil
  :defer t
  :config (winner-mode +1)
  :general
  (kbd+
    :infix "w"
    "u" '(winner-undo :wk "undo")
    "r" '(winner-redo :wk "redo")))

;;; keys
(after+ 'evil
  (general-def
    :prefix-map 'wm+hercules
    "h" '(evil-window-left :wk "move left")
    "l" '(evil-window-right :wk "move right")
    "j" '(evil-window-down :wk "move down")
    "k" '(evil-window-up :wk "move up")))

(after+ 'hercules
  (hercules-def
   :toggle-funs #'wm+hercules
   :keymap 'wm+hercules
   :transient t))

(kbd+
  :infix "w"
  "." '(wm+hercules :wk "hercules")
  "s" '(split-window-vertically :wk "split horizontally")
  "v" '(split-window-horizontally :wk "split vertically")
  "d" '(delete-window :wk "delete window")
  "k" '(kill-buffer-and-window :wk "kill window")
  "m" '(delete-other-windows :wk "maximize window")
  "f" '(maximize-window :wk "focus window"))

(provide 'wm+)
;;; wm+.el ends here
