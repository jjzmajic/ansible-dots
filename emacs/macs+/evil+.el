;;; evil+.el -*- lexical-binding: t; -*-

;;; funs
(defun evil+visual-right nil
  "vnoremap < <gv"
  (interactive)
  (evil-shift-right (region-beginning) (region-end))
  (evil-normal-state)
  (evil-visual-restore))

(defun evil+visual-left nil
  "vnoremap > >gv"
  (interactive)
  (evil-shift-left (region-beginning) (region-end))
  (evil-normal-state)
  (evil-visual-restore))

;;; packages
(use-package evil
  :defer nil
  :init
  (setq evil-disable-insert-state-bindings t
        evil-want-keybinding nil
        evil-want-integration t
        evil-shift-width 2)
  :config (evil-mode +1)
  :general
  (general-def [escape] #'keyboard-escape-quit)
  (general-def :keymaps 'evil-visual-state-map
    [remap evil-shift-right] #'evil+visual-right
    [remap evil-shift-left] #'evil+visual-left)
  (kbd+ "fw" '(evil-write-all :wk "write all"))
  (kbd+ :infix "j"
    "f" '(evil-jump-forward :wk "jump back")
    "b" '(evil-jump-backward :wk "jump forward")))

(after+ 'evil
;;; objects
  (use-package evil-textobj-line)
  (use-package evil-indent-plus
    :config (evil-indent-plus-default-bindings))
  (use-package evil-textobj-entire
    :config
    (general-def
      :keymaps '(evil-outer-text-objects-map
                 evil-inner-text-objects-map )
      evil-textobj-entire-key
      #'evil-entire-entire-buffer))
;;; verbs
  (use-package evil-lion)
  (use-package evil-exchange
    :config (evil-exchange-install))
  (use-package evil-commentary
    :config (evil-commentary-mode))
  (use-package evil-surround
    ;; do not use :general
    :config
    (general-def
      :states 'visual
      :keymaps 'evil-surround-mode-map
      "s" #'evil-surround-region
      "S" #'evil-substitute)
    :config (global-evil-surround-mode +1))
;;; other
  (use-package evil-collection
    :config (evil-collection-init)))

(provide 'evil+)
;;; evil+.el ends here
