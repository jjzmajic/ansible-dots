;;; lua+.el -*- lexical-binding: t; -*-

(use-package lua-mode
  :defer t)

(provide 'lua+)
;;; lua+.el ends here
