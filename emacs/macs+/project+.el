;;; project+.el -*- lexical-binding: t; -*-

;;; packages
(use-package projectile
  :config (projectile-mode +1)
  :general (kbd+ "pr" '(projectile-replace
                        :wk "project replace")))

(use-package helm-projectile
  :after helm+
  :config (helm-projectile-on)
  (when helm+fuzzy (setq helm-projectile-fuzzy-match t))
  :general
  (kbd+
    :infix "p"
    "p" '(helm-projectile-switch-project :wk "projects")
    "f" '(helm-projectile-find-file :wk "project file")
    "d" '(helm-projectile-find-dir :wk "project dir")
    "b" '(helm-projectile-switch-to-buffer :wk "project buffer")
    "s" '(helm-projectile-ag :wk "search project")))

(provide 'project+)
;;; project+.el ends here
