;;; prose+.el -*- lexical-binding: t; -*-

(use-package mw-thesaurus
  :general
  (kbd+ "xd"
    '(mw-thesaurus-lookup-at-point
      :wk "definition")))

(use-package powerthesaurus
  :general
  (kbd+ "xt"
    '(powerthesaurus-lookup-word-dwim
      :wk "thesaurus")))

(provide 'prose+)
;;; prose+.el ends here
