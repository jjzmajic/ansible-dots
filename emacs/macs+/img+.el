;;; img+.el -*- lexical-binding: t; -*-

(use-package image-mode
  :straight nil
  :general
  (general-def :keymaps 'image-mode-map
    :states '(normal visual)
    "+" #'image-increase-size
    "=" #'image-increase-size
    "-" #'image-decrease-size))

(provide 'img+)
;;; img+.el ends here
