;;; golang+.el -*- lexical-binding: t; -*-
(use-package magit
  :init (setq transient-mode-line-format nil)
  :general
  (kbd+
    :infix "g"
    "c" '(magit-clone :wk "clone")
    "g" '(magit-status :wk "magit")))

(use-package evil-magit
  :if (featurep 'evil+)
  :after magit)

(use-package magit-todos
  :init (setq magit-todos-exclude-globs '("*.csv"))
  :ghook 'magit-status-mode-hook)

(use-package with-editor
  :general
  (kbd+local
    :keymaps 'with-editor-mode-map
    "," '(with-editor-finish :wk "commit")
    "c" '(with-editor-cancel :wk "cancel")))

(use-package diff-hl
  :commands diff-hl-magit-post-refresh
  :init (setq diff-hl-side 'left)
  :hook ((prog-mode . diff-hl-mode)
         (magit-post-refresh . diff-hl-magit-post-refresh))
  :config
  (global-diff-hl-mode)
  (diff-hl-margin-mode)
  (diff-hl-dired-mode)
  (diff-hl-flydiff-mode))

(use-package gitignore-mode
  :defer t
  :config
  (after+ 'completion
    (compdef
     :modes #'gitignore-mode
     :company #'company-files)))

(use-package git-link
  :general (kbd+ "gl" #'git-link))

(provide 'git+)
;;; golang+.el ends here
