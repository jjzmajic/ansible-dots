;;; `(file-name-nondirectory buffer-file-name)` -*- lexical-binding: t; -*-

$0

(provide '`(file-name-base buffer-file-name)`)
;;; `(file-name-nondirectory buffer-file-name)` ends here
