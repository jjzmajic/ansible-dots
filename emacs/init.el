;;; init.el -*- lexical-binding: t; -*-

;;; vars
(setq user-emacs-directory (file-name-directory load-file-name)
      initial-major-mode #'fundamental-mode)

;;; modules
(defvar init+core
  '(
    ;; +++
    package+
    kbd+
    util+
    rice+
    ;; +++
    ))

(defvar init+glue
  '(
    ;; +++
    dirs+
    modes+
    ;; +++
    ))

(defvar init+modules
  '(
    ;; +++
    bibtex+
    build+
    cl+
    clojure+
    completion+
    cpp+
    crystal+
    data+
    dbg+
    dired+
    docker+
    editor+
    elisp+
    emacs+
    email+
    ess+
    evil+
    format+
    gif+
    git+
    golang+
    helm+
    img+
    latex+
    lint+
    lsp+
    lua+
    nav+
    nim+
    org+
    pdf+
    progrice+
    project+
    prose+
    python+
    racket+
    rust+
    shell+
    snippets+
    templates+
    term+
    transient+
    wm+
    ;; +++
    ))

(defun init+require (modules)
  (dolist (module modules)
    (require module)))

(defmacro init+turbo (&rest body)
  (declare (indent 1) (debug t))
  `(let ((gc-cons-threshold most-positive-fixnum)
         (inhibit-message (not init-file-debug)))
     ,@body))

(defun init+cdr nil
  (init+turbo
      (init+require init+glue)
    (init+require init+modules)
    (maybe+ #'benchmark-init/deactivate)))

;;; init
(add-to-list 'load-path (concat user-emacs-directory "macs+/"))
(init+turbo (init+require init+core))
(if (or noninteractive init-file-debug (daemonp))
    (init+cdr)
  (hook+ 'pre-command-hook
         #'init+cdr nil nil t))

(provide 'init)
;;; init.el ends here

;; Local Variables:
;; byte-compile-warnings: (not free-vars lexical unresolved)
;; flycheck-disabled-checkers: (emacs-lisp-checkdoc)
;; End:
